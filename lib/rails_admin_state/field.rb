require 'builder'

module RailsAdmin
  module Config
    module Fields
      module Types
        class State < RailsAdmin::Config::Fields::Base
          # Register field type for the type loader
          RailsAdmin::Config::Fields::Types::register(self)
          include RailsAdmin::Engine.routes.url_helpers

          register_instance_option :pretty_value do

            @state_machine_options = ::RailsAdminState::Configuration.new @abstract_model
            v = bindings[:view]
            obj = bindings[:object]
            c = bindings[:controller]

            self.class.pretty_value_from_params(name, @abstract_model, obj, c.try(:authorization_adapter), v, @state_machine_options, read_only)
          end

          register_instance_option :formatted_value do
            form_value
          end

          register_instance_option :form_value do
            @state_machine_options = ::RailsAdminState::Configuration.new @abstract_model
            c = bindings[:controller]
            v = bindings[:view]

            state = bindings[:object].send(name)
            state_class = @state_machine_options.state(state)
            s = bindings[:object].class.state_machines[name.to_sym].states[state.to_sym]
            ret = [
              '<div class="badge ' + state_class + '">' + s.human_name + '</div>',
              '<div style="height: 10px;"></div>'
            ]

            empty = true
            unless read_only
              events = bindings[:object].class.state_machines[name.to_sym].events
              bindings[:object].send("#{name}_events".to_sym).each do |event|
                next if @state_machine_options.disabled?(event)
                unless bindings[:controller].try(:authorization_adapter).nil?
                  adapter = bindings[:controller].authorization_adapter
                  next unless (adapter.authorized?(:state, @abstract_model, bindings[:object]) && (adapter.authorized?(:all_events, @abstract_model, bindings[:object]) || adapter.authorized?(event, @abstract_model, bindings[:object])))
                end
                empty = false
                event_class = @state_machine_options.event(event)
                ret << bindings[:view].link_to(
                  events[event].human_name,
                  '#',
                  'data-attr' => name,
                  'data-event' => event,
                  class: "state-btn btn btn-sm #{event_class}",
                  style: 'margin-bottom: 5px;'
                )
              end
            end
            unless empty
              ret << bindings[:view].link_to(
                I18n.t('admin.state_machine.no_event'),
                '#',
                'data-attr' => name,
                'data-event' => '',
                class: "state-btn btn btn-outline-secondary btn-sm active",
                style: 'margin-bottom: 5px;'
              )
            end
            ('<div style="white-space: normal;">' + ret.join(' ') + '</div>').html_safe
          end

          register_instance_option :export_value do
            state = bindings[:object].send(name)
            bindings[:object].class.state_machines[name.to_sym].states[state.to_sym].human_name
          end

          register_instance_option :partial do
            :form_state
          end

          register_instance_option :read_only do
            false
          end

          register_instance_option :allowed_methods do
            [method_name, (method_name.to_s + '_event').to_sym]
          end

          register_instance_option :multiple? do
            false
          end

          register_instance_option :searchable_columns do
            @searchable_columns ||= begin
              case searchable
              when true
                [{column: "#{abstract_model.table_name}.#{name}", type: :string}]
              when false
                []
              end
            end
          end

          def self.pretty_value_from_params name, abstract_model, obj, authorization_adapter, view, state_machine_options, read_only = false
            state = obj.send(name)
            state_class = state_machine_options.state(state)
            s = obj.class.state_machines[name.to_sym].states[state.to_sym]
            ret = [
              '<div class="badge ' + state_class + '">' + s.human_name + '</div>',
              '<div style="height: 10px;"></div>'
            ]
            unless read_only
              events = obj.class.state_machines[name.to_sym].events
              obj.send("#{name}_events".to_sym).each do |event|
                next if state_machine_options.disabled?(event)
                unless authorization_adapter.nil?
                  adapter = authorization_adapter
                  next unless (adapter.authorized?(:state, abstract_model, obj) && (adapter.authorized?(:all_events, abstract_model, obj) || adapter.authorized?(event, abstract_model, obj)))
                end
                event_class = state_machine_options.event(event)
                ret << ActionController::Base.helpers.link_to(
                  events[event].human_name,
                  view.state_path(model_name: abstract_model, id: obj.id, event: event, attr: name),
                  method: :post,
                  class: "btn btn-sm #{event_class}",
                  style: 'margin-bottom: 5px;',
                  onclick: <<-END.strip_heredoc.gsub("\n", ' ').gsub(/ +/, ' ')
                    event.preventDefault();
                    event.stopPropagation();
                    var $t = $(this);
                    const token = document.getElementsByName(
                      'csrf-token'
                    )[0].content;
                    var old_html = $t.html();
                    $t.html("<i class='fa fa-spinner fa-spin'></i>");
                    $.ajax({
                      type: "POST",
                      url: $t.attr("href"),
                      data: {ajax:true},
                      headers: {'X-CSRF-Token': token},
                      success: function(r) {
                        $t.parent().parent().html(r.html);
                      },
                      error: function(e) {
                        $t.html(old_html);
                        if(e.responseJSON && e.responseJSON.error) {
                          alert(e.responseJSON.error);
                        } else {
                          alert(e.responseText);
                        }
                      }
                    });
                    return false;
                  END
                )
              end
            end
            ('<div style="white-space: normal;">' + ret.join(' ') + '</div>').html_safe
          end
        end
      end
    end
  end
end
